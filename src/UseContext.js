import React from "react";

// create a Context object
// a context object as the name suggest is a data type of an object that can be used to store information that can be shared to other components within the app

/* 
A context object is a different approach to passing information between components and it allows us to have easier access by avoiding the use of prop-drilling.
*/

const UserContext = React.createContext() //state repo


// The provider component allows other component to consume or use the context object and supply the necessary information needed to the context object
export const UserProvider = UserContext.Provider //state delivery

export default UserContext;