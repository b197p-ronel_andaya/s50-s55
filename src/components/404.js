import { Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
function PageNotFound() {
  return (
    <Row>
      <Col className="p-5">
        <h1>Page not Found!</h1>
        <p>Go back to safety</p>
        <Button variant="primary" as={Link} to="/">Home</Button>
      </Col>
    </Row>
  );
}

export default PageNotFound;
