import { Fragment, useEffect,useState } from "react";
import CourseCard from "../components/CourseCard";
// import coursesData from "../data/CoursesData";

export default function Courses() {

  const [courses, setCourses] = useState([]);
  // const courses = coursesData.map((course) => {
  //   return <CourseCard key={course.id} courseProp={course} />;
  // });
  useEffect(()=>{
    fetch(`${process.env.REACT_APP_API_URL}/courses`).then(res=>res.json()).then(data=>{
      console.log(data);
      setCourses(data.map(course=>{
        return (
          <CourseCard key={course._id} courseProp={course} />
        )
      }))
    })
  },[]);

  return (
    <Fragment>
      <h1 className="text-center my-3">Courses</h1>
      {courses}
    </Fragment>
  );
}
