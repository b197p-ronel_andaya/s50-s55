import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UseContext";

export default function Logout() {
  // Consume the UserContext object and destructure it to access the setUser function and unsetUser function from the Context Provider
  const { unsetUser, setUser } = useContext(UserContext);

  unsetUser();
  useEffect(() => {
    setUser({ id: null });
  }, [setUser]);

  return <Navigate to="/login" />;
}
